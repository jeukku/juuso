---
title: Transition
date: 2018-11-09 22:05:09 +0000
layout: page
permalink: "/transition/"

---
Work in progress!

## FLOSS

We need decentralized free/libre open source software and platforms to support our actions.

## TZM

The Zeitgeist movement is an organization that educates people to realize a new train of thought.

Here is a suggestion how TZM should be organized [https://docs.google.com/document/d/171FypJgcynUBrg1EX5zzkmVGp1Dn_XsMlde_j6r_qU0/edit#](https://docs.google.com/document/d/171FypJgcynUBrg1EX5zzkmVGp1Dn_XsMlde_j6r_qU0/edit# "https://docs.google.com/document/d/171FypJgcynUBrg1EX5zzkmVGp1Dn_XsMlde_j6r_qU0/edit#")

## rbesolutions.org

[rbesolutions.org](rbesolutions.org "rbesolutions.org")

This ia database to put together all the information we need to build houses and automation, grow food, harness energy and so on.

### Spiral dynamic/Integral theory

This is probably very important for TZM to learn and use.

I currently think of it as a human/society development model that helps you understand how people think and act. TZM is talking about things that are transitioning us to the next "level" of development. It takes time but societies has move forward if we wan't to survive.

[Full size AQAL map](http://integral-life-home.s3.amazonaws.com/SteveSelf-Altitude.jpg "Full size AQAL map")

![](/uploads/small-aqal.png)

## Koto coop

[http://kotokoop.org](http://kotokoop.org "http://kotokoop.org")

> We will start an co-op that will fund and set up “units” where  members can live ecologically without barter, improve their skills and  plans which help expand the way of living and gather resources needed to  set up a next unit. Number of units will grow exponentially and money  needed for the next unit decrease because resources will be produced  more and more inside co-op.
>
> Participation share will grant you right to live in any of the units  and move to another unit at will. Price of the share is so equals to  price of the resources needed to build one apartment. Monthly cost will  be less than what people typically pay rent.

## Businesses

Different businesses that are supported by Koto coop. This is needed for things that cannot be run through Koto.

Businesses could be owned by Koto and TZM together, organizations could buy services from there businesses or the other way around. It is a mix of things that needs to be thought trough.

## Politics

At some point we need to create a parties to have some control how politics will be used against us.

## Stage 0

### FLOSS

TZM platform specification [https://etherpad.net/p/TZM_platform_specification](https://etherpad.net/p/TZM_platform_specification "https://etherpad.net/p/TZM_platform_specification")

#### Collabthings

I'm creating a platform to share information through p2p networks and to build shared decentralized databases to let people collaborate on different things.

##### Parts

This is the current Collabthings client. It isn't really needed in the beginning, but that's what was implemented first. It is a platform to store, share in a designs of machines and  constructions, scripts and instructions how those are manufactured and  maintained.  You can take any design, edit and publish it.

[https://www.youtube.com/watch?v=MJQ2_3F4-ok](https://www.youtube.com/watch?v=MJQ2_3F4-ok "https://www.youtube.com/watch?v=MJQ2_3F4-ok")

##### Forum

Everybody knows what a forum is right? We need to create one with a decentralized platform.

##### Chat

Channels for chatting similar to discord, slack and IRC.

##### Publishing platform

I'm thinking of using tzm.community -platform ([https://tzmcommunity.github.io/docs/](https://tzmcommunity.github.io/docs/ "https://tzmcommunity.github.io/docs/")) to create a tool where you can edit and publish a web post in collabthings platform and it would be published to the web through tzm.community jekyll engine or similar.

### TZM

### Koto

### Business

## Stage 1

### FLOSS

### TZM

### Koto

### Business

## Stage 2

### FLOSS

### TZM

### Koto

### Business

## Stage 3

### FLOSS

### TZM

### Koto

### Business

hello