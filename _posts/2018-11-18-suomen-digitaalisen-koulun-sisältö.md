---
title: Suomen digitaalisen koulun sisältö
date: 2018-11-18 12:09:46 +0000
layout: post

---
# Taustaa

Hyvää digitaalista koulujärjestelmää ei voi luoda niin, että korvataan vanhan tyylisestä opetuksesta joitain työkaluja digitaalisilla järjestelmillä. Nykyinen "digi-intoilu" on juuri sitä mitä teknologiaa ymmärtävät ihmiset eivät kannata.

Kirjakustantamoiden tuottamat sovellusprojektit tietenkin epäonnistuvat koska kirjakustantamot eivät ole softataloja eivätkä ne tarpeeksi nopeasti sellaisiksi muutu. Kustantamojen pitäisi jatkossakin keskittyä siihen missä ovat hyvä eli sisällön tuottamiseen.

Kaiken sisällön tulisi olla vapaasti käytettävää (free/libre open software) jotta vältetään se että joka vuosi pitää ostaa uusi "kirjasarja" kustantamolta. Kustantamoilta tulisi ostaa päivityksiä vanhaan sisältöön ja uutta sisältöä vapaasti käytettäväksi.

Tarvitaan siis ajatuksia vähän laatikon ulkopuolelta.

Kuvaan tähän toteutuksen ja prosessin. En ole mikään opetuksen asiantuntija joten varmasti menee moni asia pieleen. Älä hermostu vaan korjaa. Kirjaan eri osioihin "teknologia" otsikon alla asiat jotka voi hypätä yli jos ei ole teknologiasta kiinnostunut.

# Sisällölle tietokanta

Tämän nimi tulisi ehkä olla jonkinlainen kirjasto.

## Vapaasti käytettävissä

Sisältö lisenssoidaan GPLv2 lisenssillä, mikä tarkoittaa sitä että jokainen voi tehdä muutoksia omaan versioonsa sisällöstä, mutta kaikki muutokset pitää julkaista internettiin muiden käytettäväksi.

## Kaikki voivat luoda omia versiota

Eri versioita voidaan yhdistellä ja lopuksi julkaista "virallinen" versio joka on oletuksena kaikkien käytössä.

Eri kouluille voivat opettajat tehdä omia versiota ja toiset koulut voivat valita käyttävätkö alkuperäistä vai muokattua versiota.

## Kaikki voivat luoda uutta sisältöä

Samalla tavalla kuin versioiden luominen uuden sisällön tuottaminen on helppoa.

Käytännössä uuden materiaalinen tapahtuu samalla tavalla kuin uuden wikipedia sivun luominen.

Oppimiskokonaisuuksissa viitataan sisältöön yksilöivällä osoitteella. Jos luot itse uutta sisältöä, osoiteessa on mukana käyttäjänimesi. 

## Myös tehtävät tietokantaan

Tehtävien (ja kokeiden) tallentamiseen tietokantaan pitää sopia jonkinnäköinen standardi. Tehtävät ovat samanlaista tekstimuotoista sisältöä kuin muukin materiaali joten niistä voi jokainen tehdä omia versiota.

Tehtävien sisältö tulkitaan tietokannan ulkopuolisessa oppimisympäristössä ja vastaukset tallennetaan myös tallennetaan sinne.

Eri järjestelmä takaa sen, että arkaluontoista tietoa ei eksy julkiseen sisältötietokantaan. 

## Teknologia

Tässä parhaiten toimisi yksinkertainen wikimäinen Github pages tyylinen ratkaisu. Miksei sitten alkuvaiheessa Github pages? Jokin valtion hostaama git palvelu olisi tietenkin parempi monessa mielessä (ja huonompi monessa muussa).

Sisältö luodaan md -sivuina jotka julkaistaan automaattisesti Jekyll:n simple teemalla wikimäiseksi kokonaisuudeksi. Esimerkiksi minun versio olisi osoitteessa https://jeukku.github.io/opetus.

Isommat binääri -tiedostot voisi jakaa vaikka IPFS verkon kautta. Kaistaa ei tarvitsisi niin paljon ja toimii varmasti jos koulun verkossa on "node, jossa tarvittavat tiedostot ovat jo ladattuna.  Javascriptillä on helppo tehdä toteutus jossa sisältö ladataan tiedetyltä palvelimelta jos IPFS käyttö ei onnistu. Alkuvaiheessa voisi ehkä käyttää jotain perinteisempää ratkaisua.

# Erillinen toteutus oppimisympäristölle

Oppilastietojen tallentamiseen ja edistymisen seurantaan tarvitaan erillinen järjestelmä.  

## Kirjautuminen

Jokainen kaupunki tai koulu voi valita käyttääkö yhteistä instanssia vai käytetäänkö omaa. Instanssi voi toki olla esimerkiksi kahden kaupungin yhteishanke.

Kaikki sisältö vaatii kirjautumisen ja julkisesta tietokannasta vain haetaan sisältöä.

## Kurssit ja muu sisältö

Oppikokonaisuudet luodaan julkiseen tietokantaan jotta myös muut niistä hyötyvät. Oppimisympäristöön sitten valitaan ne kurssit ja muut sisällöt joita opettajille ja oppilaille halutaan näyttää. 

## Sisältö ja tehtävät

Nykyisissä toteutuksissa monesti on se ongelma, että kirja-materiaalin (eli sisällön) ja tehtävien välillä liikkuminen on hankalaa ja niitä ei saa edes yhtäaikaa näkyviin.

Oppimisympäristön sovelluksessa tulisi voida jakaa näyttö kahteen osaan niin, että sisältö näkyy toisessa osassa ja tehtäviä voi tehdä toisessa.

## Oppimisen seuranta

Opettajille ja oppilaille tulisi olla jonkin verran erilaisia näkymiä. Esimerkiksi opettajalle olisi näkymä jossa näkyy koko luokan oppilaiden edistyminen ja esimerkiski tekemättömät tehtävät. Oppilas ja vanhemmat tietenkin näkisi vastaavasti omansa.

## Ulkoasu

Ulkoasun kanssa ei saa lähteä "kikkailemaan". On mukavaa jos ulkoasu on tarpeeksi moderni ja nätti, mutta selkeys on pääasemassa. 

## Teknologia

Melko tavallinen webbisovellus. Sisältö on melko staattista joten todennäköisesti sitä kannattaa tulkita javascript sovelluksella ja esim vastaukset tehtäviin tallentaa API:n avulla johonkin tietokanta -toteutukseen.

Sisältötietokannan jokaisesta sivusta voinee luoda automaattisesti JSON version joten oppimisympäristön puolella on mahdollista käyttää joko tietokannan sivua suoraan iframessa tai tulkita JSON -tietoa.  Tehtävien kohdalla ainakin JSON on varmasti mukavampi toteuttaa.

Ainoa hankala kohta joka tulee mieleen on tehtävien vastausten tallentaminen. Voi olla, että ongelmia esiintyy jos käytetään eri versioita sisällöstä.

# Mitäs muuta

Unohtuiko jotain? Pistä postia allekirjoittaneelle juuso@vilmunen.net jos haluat.

Tämä on tietenkin GPLv2 lisenssoitu teksti joten tee oma versio jos huvittaa :) [https://github.com/jeukku/juuso/blob/master/_posts/2018-11-18-suomen-digitaalisen-koulun-sis%C3%A4lt%C3%B6.md](https://github.com/jeukku/juuso/blob/master/_posts/2018-11-18-suomen-digitaalisen-koulun-sis%C3%A4lt%C3%B6.md "https://github.com/jeukku/juuso/blob/master/_posts/2018-11-18-suomen-digitaalisen-koulun-sis%C3%A4lt%C3%B6.md")

Onkohan vastaava toteutus jo olemassa? Toteutetaanko?