---
title: Veganism and TZM
date: 2019-09-28 13:45:00 +0000
layout: post

---
# Veganism and TZM

Veganism is all about reducing unnecessary suffering. Vegans extend the concept of suffering to all animals including humans. We accept that understanding the suffering we create and compassion we feel towards all beings are more important reasons to make decisions in life than our slight discomfort, taste buds or habits. 

Understanding that the current state of humanity's sociocultural evolution is creating structures of societies and thought that lead to destroying earth's ability to support life and thus extreme suffering, we choose to support people move past their old ways of thinking and help them find compassion and happiness.

We recognize that humanity has reached a level of science and technology where we are not limited by scarcity anymore and realize that most of what people want are not the result of their needs, but artificially created by incentive to alter fears and thoughts people have and environments they live in. 

I hope we choose to not be just content understanding all this but find ways to master our lives to make future better, to not only be the people we want to be but to do it in our daily lives and realize that saving us will require some sacrifice and a lot of wisdom, love and compassion.
